package model;

/**
 * Created by Adela on 16.05.2017.
 */
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;


public class CurrentUser extends User {

    private UserEntity userEntity;

    public CurrentUser(UserEntity userEntity) {
        super(userEntity.getUsername()
                , userEntity.getPassword()
                , AuthorityUtils.createAuthorityList(userEntity.getRole()));

        this.userEntity = userEntity;
    }

    public UserEntity getUser() {
        return userEntity;
    }

    public Long getId() {
        return userEntity.getId();
    }

    public String getRole() {
        return userEntity.getRole();
    }

}