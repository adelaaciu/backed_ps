package model;

import dto.DoctorDto;
import dto.HospitalDto;
import dto.SectorDto;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Adela on 07.03.2017.
 */
@Getter
@Setter
@Entity
@Table(name = "hospitals")
public class HospitalEntity extends BaseEntity<HospitalDto> {
    @NotNull
    private String name;
    private String city;
    private String address;

    @OneToMany(mappedBy = "hospital", fetch = FetchType.EAGER)
    private Set<AppointmentEntity> appointments = new HashSet<>();
    @OneToMany(mappedBy = "hospital", fetch = FetchType.EAGER)
    private Set<DoctorEntity> doctors = new HashSet<>();
    @OneToMany(mappedBy = "hospital", fetch = FetchType.EAGER)
    private Set<SectorEntity> sectors = new HashSet<>();

    public HospitalEntity() {
    }


    @Override
    public HospitalDto toDto() {
        HospitalDto dto = new HospitalDto();
        dto.setName(this.name);
        dto.setAddress(this.address);
        dto.setCity(this.city);
//        dto.setDoctorList(convertDoctorsListToDto());
//        dto.setSectorList(convertSectorToDto());
        return dto;
    }

    @Override
    public void fromDto(HospitalDto dto) {
        this.setCity(dto.getCity());
        this.setAddress(dto.getAddress());
        this.setName(dto.getName());
        this.setDoctors(convertToDoctorEntityList(dto.getDoctorList()));
        this.setSectors(convertToSectorEtityList(dto.getSectorList()));
    }


    private Set<SectorDto> convertSectorToDto() {
        Set<SectorDto> dtos = new HashSet<>();

        sectors.forEach(x -> dtos.add(x.toDto()));

        return dtos;
    }

    private Set<SectorEntity> convertToSectorEtityList(Set<SectorDto> sectors) {
        Set<SectorEntity> sectorEntities = new HashSet<>();
        sectors.forEach(x -> {
            SectorEntity sectorEntity = new SectorEntity();
            sectorEntity.fromDto(x);
            sectorEntities.add(sectorEntity);
        });
        return sectorEntities;
    }

    private Set<DoctorDto> convertDoctorsListToDto() {
        Set<DoctorDto> dtos = new HashSet<>();

        doctors.forEach(x -> dtos.add(x.toDto()));

        return dtos;
    }

    private Set<DoctorEntity> convertToDoctorEntityList(Set<DoctorDto> doctorDtos) {
        Set<DoctorEntity> doctors = new HashSet<>();
        doctorDtos.forEach(x -> {
            DoctorEntity doctorEntity = new DoctorEntity();
            doctorEntity.fromDto(x);
            doctors.add(doctorEntity);
        });
        return doctors;
    }

    @Override
    public String toString() {
        return "HospitalEntity{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
