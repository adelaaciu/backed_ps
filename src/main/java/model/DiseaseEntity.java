package model;

import dto.DiseaseDto;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * Created by Adela on 07.03.2017.
 */

@Entity
@Table(name = "diseases")
public class DiseaseEntity extends BaseEntity<DiseaseDto> {
    @NotNull(message = "DiseaseEntity is null")
    private String name;
    private String category;
    private ArrayList<String> symptoms;

    @ManyToOne
    private DrugEntity drugEntity;

    public DiseaseEntity() {
    }

    @Override
    public DiseaseDto toDto() {
        DiseaseDto dto = new DiseaseDto();
        dto.setName(this.name);
        dto.setCategory(category);
        dto.setSymptomList(this.symptoms);

        return dto;
    }

    @Override
    public void fromDto(DiseaseDto dto) {
        this.name = dto.getName();
        this.category = dto.getCategory();
        this.symptoms = dto.getSymptomList();
    }

    public DrugEntity getDrugEntity() {
        return drugEntity;
    }

    public void setDrugEntity(DrugEntity drugEntity) {
        this.drugEntity = drugEntity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<String> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(ArrayList<String> symptoms) {
        this.symptoms = symptoms;
    }

    @Override
    public String toString() {
        return "DiseaseEntity{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", symptoms=" + symptoms +
                ", drugEntity=" + drugEntity +
                '}';
    }
}
