package model;

import dto.AppointmentDto;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 * Created by Adela on 05.05.2017.
 */
@Data
@Entity
public class AppointmentEntity extends BaseEntity<AppointmentDto> {

    String doctorName;
    @ManyToOne
    HospitalEntity hospital;
    String category;
    LocalDateTime date;

    @Override
    public AppointmentDto toDto() {
        AppointmentDto appointmentDto = new AppointmentDto();
        appointmentDto.setCategory(category);
        appointmentDto.setDoctorName(doctorName);
        appointmentDto.setHospitalName(hospital.getName());
        appointmentDto.setHospitalCity(hospital.getCity());
        appointmentDto.setCategory(category);
        return appointmentDto;
    }

    @Override
    public void fromDto(AppointmentDto dto) {
        this.doctorName = dto.getDoctorName();
        this.category = dto.getCategory();
        this.date = dto.getDate();
    }
}
