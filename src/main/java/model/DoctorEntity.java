package model;

import dto.DoctorDto;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Adela on 07.03.2017.
 */
@Getter
@Setter
@Entity
@Table(name = "doctors")
public class DoctorEntity extends BaseEntity<DoctorDto> {
//    @NotNull
    private String name;
    private String phoneNo;
//    private String email;
//    @OneToOne

//    private SectorEntity sectorEntity;
    private String startHour;
    private String endHour;
    @ManyToOne
    private HospitalEntity hospital;

    public DoctorEntity() {
    }


    @Override
    public DoctorDto toDto() {
        DoctorDto dto = new DoctorDto();
        dto.setName(name);
        dto.setEndHour(endHour);
        dto.setPhoneNo(phoneNo);
        dto.setStartHour(startHour);

        return dto;
    }

    @Override
    public void fromDto(DoctorDto dto) {
        this.name = dto.getName();
        this.endHour = dto.getEndHour();
        this.phoneNo = dto.getPhoneNo();
        this.endHour = dto.getStartHour();
    }

    @Override
    public String toString() {
        return "DoctorEntity{" +
                "name='" + name + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", startHour='" + startHour + '\'' +
                ", endHour='" + endHour + '\'' +
                '}';
    }
}
