package model;

import dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by Adela on 12.03.2017.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Entity
@Table(name = "users")
public class UserEntity extends BaseEntity<UserDto>{

    @NotNull(message = "User can't be null")
    @Column(unique = true)
    private String username;
    @NotNull(message = "Password can't be null")
    private String password;
    private String email;
    private String role;

    @Override
    public UserDto toDto() {
        UserDto dto = new UserDto();
        dto.setUsername(username);
        dto.setPassword(password);
        return dto;
    }

    @Override
    public void fromDto(UserDto dto) {
        this.setUsername(dto.getUsername());
        this.setPassword(dto.getPassword());
        this.role = "USER";
    }
}
