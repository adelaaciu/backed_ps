package model;

import dto.PharmacyDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by Adela on 14.03.2017.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "pharmacies")
public class PharmacyEntity extends BaseEntity<PharmacyDto> {
    private String name;
    private String startHour;
    private String endHour;
    private String city;
    private String address;

    public PharmacyEntity() {
    }

    @Override
    public PharmacyDto toDto() {
        PharmacyDto dto = new PharmacyDto();
        dto.setName(name);
        dto.setCity(city);
        dto.setAddress(address);
        dto.setEndHour(endHour);
        dto.setStartHour(startHour);

        return dto;
    }

    @Override
    public void fromDto(PharmacyDto dto) {
        this.name = dto.getName();
        this.startHour = dto.getStartHour();
        this.endHour = dto.getEndHour();
        this.city = dto.getCity();
        this.address = dto.getAddress();
    }
}
