
package repository;

import model.DrugEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Repository
public interface DrugRepository extends PagingAndSortingRepository<DrugEntity, Long> {
    DrugEntity findByName(String name);
    List<DrugEntity> findByCategory(String category);
    List<DrugEntity> findByNameContaining(String name);
}
