package repository;

import model.PharmacyEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@Repository
public interface PharmacyRepository extends PagingAndSortingRepository<PharmacyEntity,Long> {
    List<PharmacyEntity> findByCity(String city);
    List<PharmacyEntity> findByName(String name);
}
