package repository;

import model.DiseaseEntity;
import model.DrugEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Repository
public interface DiseaseRepository extends PagingAndSortingRepository<DiseaseEntity, Long> {
    DiseaseEntity findByName(String name);
    List<DiseaseEntity> findAllByNameContaining(String name);
    List<DiseaseEntity> findAllByCategory(String category);

    DiseaseEntity findByDrugEntity(DrugEntity drugEntity);
}
