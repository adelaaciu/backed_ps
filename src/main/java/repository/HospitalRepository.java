package repository;

import model.DoctorEntity;
import model.HospitalEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by Adela on 07.03.2017.
 */
@Repository
public interface HospitalRepository extends PagingAndSortingRepository<HospitalEntity, Long> {
    List<HospitalEntity> findByCity(String city);
    Set<HospitalEntity> findByName(String name);
    HospitalEntity findByNameAndCity(String name, String city);
}
