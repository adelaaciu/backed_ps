package repository;

import model.SectorEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Adela on 07.03.2017.
 */
@Repository
public interface SectorRepository extends PagingAndSortingRepository<SectorEntity, Long> {
    SectorEntity findSectorByName(String name);
}
