package dto;
import lombok.Data;
/**
 * Created by Adela on 14.03.2017.
 */
@Data
public class UserDto {
    private String username;
    private String password;

    public UserDto() {
    }
}
