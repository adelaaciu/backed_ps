package dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 * Created by Adela on 05.05.2017.
 */
@Data
public class AppointmentDto {

    String doctorName;
    String hospitalName;
    String hospitalCity;
    String category;
    LocalDateTime date;

}

