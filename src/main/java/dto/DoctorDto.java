package dto;

import lombok.Data;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class DoctorDto {
    private String name;
    private String phoneNo;
//    private SectorDto sector;
    private String startHour;
    private String endHour;

    public DoctorDto() {
    }
}
