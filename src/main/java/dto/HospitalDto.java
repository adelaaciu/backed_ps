package dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class HospitalDto {
    private String name;
    private String city;
    private String address;
    private Set<DoctorDto> doctorList;
    private Set<SectorDto> sectorList;

    public HospitalDto() {
    }

}
