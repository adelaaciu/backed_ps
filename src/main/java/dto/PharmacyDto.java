package dto;

import lombok.Data;

/**
 * Created by Adela on 14.03.2017.
 */
@Data
public class PharmacyDto {
    private String name;
    private String startHour;
    private String endHour;
    private String city;
    private String address;

    public PharmacyDto() {
    }
}
