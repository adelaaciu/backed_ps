package dto;

import dto.PharmacyDto;
import export.Convertor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Adela on 28.05.2017.
 */
public class Context {
    private  final Convertor convertor;

    public Context(Convertor convertor) {
        this.convertor = convertor;
    }

    public File convertToFormat(List<PharmacyDto> pharmacies) throws IOException {
        return this.convertor.convert(pharmacies);
    }
}
