package dto;

import lombok.Data;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class SectorDto {
    private String name;
    private String level;
    private int chambers;
    public static int NO_MAX_OF_PERSONS = 50;
   // private HospitalDto hospital;

    public SectorDto() {
    }
}
