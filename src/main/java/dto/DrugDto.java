package dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class DrugDto {

    private String name;
    private String composition;
    private String category;
    private ArrayList<String> adverseReactions = new ArrayList<>();
    private Set<DiseaseDto> diseases = new HashSet<>();

    public DrugDto() {
    }
}
