package dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class DiseaseDto {

    private String name;
    private String category;
    private ArrayList<String> symptomList;

    public DiseaseDto() {
    }
}
