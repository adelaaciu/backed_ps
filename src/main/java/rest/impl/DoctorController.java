package rest.impl;

import dto.DoctorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import rest.api.DoctorEndpoint;
import service.api.IDoctorService;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
@CrossOrigin
public class DoctorController implements DoctorEndpoint {
    @Autowired
    private IDoctorService doctorService;

    public ResponseEntity<DoctorDto> getDoctor(@PathVariable("id") long id) {
        DoctorDto dto = doctorService.getDoctor(id);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity getDoctors(@PathVariable String hospitalName, @PathVariable String hospitalCity) {
        List<DoctorDto> doctors = doctorService.getDoctors(hospitalName, hospitalCity);

        if (doctors.size() > 0)
            return ResponseEntity.ok(doctors);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }


    public ResponseEntity<List<DoctorDto>> getAllDoctors() {
        List<DoctorDto> dtos = doctorService.getAllDoctors();

        if (dtos.size() > 0)
            return ResponseEntity.ok(dtos);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    public ResponseEntity<DoctorDto> addDoctor(@RequestBody DoctorDto dto) {
        DoctorDto doctorDto = doctorService.addDoctor(dto);

        if (doctorDto == null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    public ResponseEntity<DoctorDto> updateDoctor(@PathVariable("id") long id, @RequestBody DoctorDto dto) {
        DoctorDto doctorDto = doctorService.updateDoctor(id, dto);

        if (doctorDto != null) {
            return ResponseEntity.ok(doctorDto);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity deleteDoctor(@PathVariable("id") long id) {
        if (doctorService.canDelete(id)) {
            doctorService.deleteDoctor(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
