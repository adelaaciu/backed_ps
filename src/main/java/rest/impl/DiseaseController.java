package rest.impl;

import dto.DiseaseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.DiseaseEndpoint;
import service.api.IDiseaseService;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
@CrossOrigin
public class DiseaseController implements DiseaseEndpoint {
    @Autowired
    private IDiseaseService diseaseService;

    public ResponseEntity<DiseaseDto> getDiseaseById(@PathVariable("id") long id) {
        DiseaseDto dto = diseaseService.getDisease(id);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<DiseaseDto> getDiseaseByName(@PathVariable("name") String name) {
        DiseaseDto dto = diseaseService.getDiseaseByName(name);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity getDiseaseByNameContaining(@RequestParam("name") String name) {
        List<DiseaseDto> allByNameContaining = diseaseService.findAllByNameContaining(name);

        if (allByNameContaining != null)
            return ResponseEntity.ok(allByNameContaining);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity getDiseaseByCategory(@PathVariable("category") String category) {
        List<DiseaseDto> diseaseByCategory = diseaseService.getDiseaseByCategory(category);

        if (diseaseByCategory != null)
            return ResponseEntity.ok(diseaseByCategory);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    public ResponseEntity<List<DiseaseDto>> getAllDiseases() {
        List<DiseaseDto> dtos = diseaseService.getAllDiseases();

        if (dtos  != null)
            return ResponseEntity.ok(dtos);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<DiseaseDto> addDisease(@RequestBody DiseaseDto dto) {
        DiseaseDto diseaseDto = diseaseService.addDisease(dto);

        if (diseaseDto == null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    public ResponseEntity<DiseaseDto> updateDisease(@PathVariable("id") long id, @RequestBody DiseaseDto dto) {
        DiseaseDto diseaseDto = diseaseService.updateDisease(id, dto);

        if (diseaseDto != null) {
            return ResponseEntity.ok(diseaseDto);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity deleteDisease(@PathVariable("id") long id) {
        if(diseaseService.canDelete(id)){
            diseaseService.deleteDisease(id);
            return  ResponseEntity.status(HttpStatus.OK).build();
        }

        return  ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
