package rest.impl;

import dto.DoctorDto;
import dto.HospitalDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import rest.api.HospitalEndpoint;
import service.api.IHospitalService;

import java.util.List;
import java.util.Set;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
@CrossOrigin
public class HospitalController implements HospitalEndpoint {

    @Autowired
    private IHospitalService hospitalService;

    public ResponseEntity<HospitalDto> getHospital(@PathVariable("id") long id) {
        HospitalDto dto = hospitalService.getHospital(id);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    public ResponseEntity getHospitalsByCity(@RequestParam("hospitalCity") String hospitalCity) {
        List<HospitalDto> dtos = hospitalService.getHospitalsByCity(hospitalCity);
        if (dtos != null) {
            return ResponseEntity.ok(dtos);
        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<List<HospitalDto>> getAllHospitals() {
        List<HospitalDto> dtos = hospitalService.getAllHospitals();

        if (dtos.size() > 0)
            return ResponseEntity.ok(dtos);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    public ResponseEntity<HospitalDto> addHospital(@RequestBody HospitalDto dto) {
        HospitalDto hospitalDto = hospitalService.addHospital(dto);

        if (hospitalDto == null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);

    }

    public ResponseEntity<HospitalDto> updateHospital(@PathVariable("id") long id, @RequestBody HospitalDto dto) {
        HospitalDto hospitalDto = hospitalService.updateHospital(id, dto);

        if (hospitalDto != null) {
            return ResponseEntity.ok(hospitalDto);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    public ResponseEntity deleteHospital(@PathVariable("id") long id) {
        if (hospitalService.canDelete(id)) {
            hospitalService.deleteHospital(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity getDoctorsFromHospital(@RequestParam("hospital") String hospital,@RequestParam("hospitalCity") String hospitalCity) {
        Set<DoctorDto> dtos = hospitalService.getDoctorsFromHospital(hospital,hospitalCity);
        if(dtos.size() >0)
            return ResponseEntity.ok(dtos);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
