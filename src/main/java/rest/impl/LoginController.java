package rest.impl;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import rest.api.LoginEndpoint;

import java.security.Principal;

/**
 * Created by Adela on 22.05.2017.
 */
@RestController
@CrossOrigin
public class LoginController implements LoginEndpoint {

    public Principal user(Principal user) {
        return user;
    }
}
