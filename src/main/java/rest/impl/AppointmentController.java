package rest.impl;

import dto.AppointmentDto;
import model.AppointmentEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import rest.api.AppointmentEndpoint;
import service.api.AppointmentService;

import java.net.URI;

@RestController
@CrossOrigin
public class AppointmentController implements AppointmentEndpoint {

    private final
    AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    public ResponseEntity getAppointment(@PathVariable long appointmentId) {
        return ResponseEntity.ok(appointmentService.getAppointment(appointmentId));
    }

    public ResponseEntity addAppointment(@RequestBody AppointmentDto appointmentDto) {
        AppointmentEntity appointmentEntity = appointmentService.addAppointment(appointmentDto);
        if (appointmentEntity == null)
            return ResponseEntity.badRequest().body("Can't create appointment in this hour.");
        return ResponseEntity.created(URI.create("/" + appointmentEntity.getId())).body(appointmentEntity.toDto());
    }
}
