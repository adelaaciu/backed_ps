package rest.impl;

import dto.UserDto;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import rest.api.UserEndpoint;
import service.api.IUserService;

import java.net.URI;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
@CrossOrigin
public class UserController implements UserEndpoint {
    final IUserService userService;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity create(@RequestBody UserDto userDto) {
        UserEntity user = new UserEntity();
        user.fromDto(userDto);
        UserDto save = userService.save(user);
        return ResponseEntity.created(URI.create("/" + save.getUsername())).body(save);
    }

    @Override
    public ResponseEntity getAll() {
        return ResponseEntity.ok(userService.getAll());
    }
}
