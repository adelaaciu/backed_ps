package rest.impl;

import dto.PharmacyDto;
import export.CSVConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.PharmacyEndpoint;
import service.api.IPharmacyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@RestController
@CrossOrigin
public class PharmacyController implements PharmacyEndpoint {

    @Autowired
    private IPharmacyService pharmacyService;

    public ResponseEntity<PharmacyDto> getPharmacy(@PathVariable("id") long id) {
        PharmacyDto dto = pharmacyService.getPharmacy(id);

        return dto != null ? ResponseEntity.ok(dto) : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

//
//    public ResponseEntity getAllPharmaciesCity(@RequestParam("hospitalCity") String hospitalCity) {
//        List<PharmacyDto> dtos = pharmacyService.getPharmaciesByCity(hospitalCity);
//        if (dtos != null) {
//            return ResponseEntity.ok(dtos);
//        } else
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
//
//    }

    public ResponseEntity<List<PharmacyDto>> getAllPharmacies() {
        List<PharmacyDto> dtos = pharmacyService.getAllPharmacies();
        return dtos != null ? ResponseEntity.ok(dtos) : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<PharmacyDto> addPharmacy(@RequestBody PharmacyDto dto) {
        PharmacyDto pharmacyDto = pharmacyService.addPharmacy(dto);

        return pharmacyDto == null ? ResponseEntity.status(HttpStatus.CONFLICT)
                .build() : ResponseEntity.status(HttpStatus.CREATED).body(dto);

    }

    public ResponseEntity<PharmacyDto> updatePharmacy(@PathVariable("id") long id, @RequestBody PharmacyDto dto) {
        PharmacyDto pharmacyDto = pharmacyService.updatePharmacy(id, dto);
        return pharmacyDto == null ? ResponseEntity.status(HttpStatus.CONFLICT)
                .build() : ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    public ResponseEntity deletePharmacy(@PathVariable("id") long id) {
        if (pharmacyService.canDelete(id)) {
            pharmacyService.deletePharmacy(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    public ResponseEntity getPharmaciesByCity(@RequestParam("city") String city) throws IOException {
        List<PharmacyDto> dtos = pharmacyService.getPharmaciesByCity(city);
        if (dtos != null) {
            return ResponseEntity.ok(dtos);
        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity convert() throws IOException {
        File file = new File("C:\\Users\\Alexandru\\fi.csv");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.TEXT_PLAIN);
        httpHeaders.setContentDispositionFormData("attachment", "ce-nume-vrei-la-fisier.csv");

        InputStreamResource isr = new InputStreamResource(new FileInputStream(file));

        return new ResponseEntity(isr, httpHeaders, HttpStatus.OK);
    }
}
