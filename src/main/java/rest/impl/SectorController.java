package rest.impl;

import dto.SectorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import rest.api.SectorEndpoint;
import service.api.ISectorService;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
@CrossOrigin
public class SectorController implements SectorEndpoint {
    @Autowired
    private ISectorService sectorService;

    public ResponseEntity<SectorDto> getSector(@PathVariable("id") long id) {
        SectorDto dto = sectorService.getSector(id);
        return dto != null ? ResponseEntity.ok(dto) : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<List<SectorDto>> getAllSectors() {
        List<SectorDto> dtos = sectorService.getAllSectors();
        return dtos != null ? ResponseEntity.ok(dtos) : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<SectorDto> addSector(@RequestBody  SectorDto dto) {
        SectorDto sectorDto = sectorService.addSector(dto);
        return sectorDto == null ? ResponseEntity.status(HttpStatus.CONFLICT)
                .build() : ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    public ResponseEntity<SectorDto> updateSector(@PathVariable("id") long id, @RequestBody SectorDto dto) {
        SectorDto sectorDto = sectorService.updateSector(id, dto);
        return sectorDto != null ? ResponseEntity.ok(sectorDto) : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity deleteSector(@PathVariable("id") long id) {
        if (sectorService.canDelete(id)) {
            sectorService.deleteSector(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
