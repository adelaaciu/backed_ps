package rest.api;

import dto.DrugDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Adela on 12.03.2017.
 */
@RequestMapping("/drugs")
@CrossOrigin
public interface DrugEndpoint {
    @GetMapping(value = "/{id}")
    ResponseEntity getMedicineById(long id);

    @GetMapping(value = "/{name}")
    ResponseEntity getMedicineByName(String name);

    @GetMapping(value = "/category/{category}")
    ResponseEntity getMedicineByCategory(String category);

    @GetMapping(value = "/disease/{name}")
    ResponseEntity getMedicineByDisease(String name);

    @GetMapping(value = "/list")
    ResponseEntity getAllMedicines();

    @PostMapping(value = "/")
    ResponseEntity addMedicine(DrugDto dto);

    @PutMapping(value = "/{id}")
    ResponseEntity updateMedicine(long id, DrugDto dto);

    @DeleteMapping(value = "/{id}")
    ResponseEntity deleteMedicine(long id);
}
