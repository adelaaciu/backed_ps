package rest.api;

import dto.DiseaseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Adela on 12.03.2017.
 */
@RequestMapping("/diseases")
@CrossOrigin
public interface DiseaseEndpoint {


    @GetMapping(value = "/{id}")
    ResponseEntity getDiseaseById(long id);

    @GetMapping(value = "/name/{name}")
    ResponseEntity getDiseaseByName(String name);

    @GetMapping
    ResponseEntity getDiseaseByNameContaining(String name);

    @GetMapping(value = "/category/{category}")
    ResponseEntity getDiseaseByCategory(String category);


    @GetMapping(value = "/list")
    ResponseEntity getAllDiseases();

    @PostMapping
    ResponseEntity addDisease(DiseaseDto dto);

    @PutMapping(value = "/{id}")
    ResponseEntity updateDisease(long id, DiseaseDto dto);

    @DeleteMapping(value = "/{id}")
    ResponseEntity deleteDisease(long id);
}
