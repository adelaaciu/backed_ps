package rest.api;

import dto.HospitalDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Adela on 12.03.2017.
 */
@RequestMapping("/hospitals")
@CrossOrigin
public interface HospitalEndpoint {
    @GetMapping(value = "/{id}")
    ResponseEntity getHospital(long id);

    @GetMapping
    ResponseEntity getHospitalsByCity(String hospitalCity);

    @GetMapping(value = "/list")
    ResponseEntity getAllHospitals();

    @PostMapping(value = "/")
    ResponseEntity addHospital(HospitalDto dto);

    @PutMapping(value = "/{id}")
    ResponseEntity updateHospital(long id, HospitalDto dto);

    @DeleteMapping(value = "/{id}")
    ResponseEntity deleteHospital(long id);

    @GetMapping(value = "/doctors")
    ResponseEntity getDoctorsFromHospital(String hospital,String hospitalCity);


}
