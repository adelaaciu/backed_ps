package rest.api;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;


/**
 * Created by Adela on 22.05.2017.
 */
@RestController
@RequestMapping
@CrossOrigin
public interface LoginEndpoint {

    @RequestMapping("/log")
    Principal user(Principal user);

}
