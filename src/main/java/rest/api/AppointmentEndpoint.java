package rest.api;

import dto.AppointmentDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Adela on 5/21/2017.
 */
@RequestMapping("/appointments")
public interface AppointmentEndpoint {

    @GetMapping("/{appointmentId}")
    ResponseEntity getAppointment(@PathVariable long appointmentId);

    @CrossOrigin(origins = "*", maxAge = 3600)
    @PostMapping("/create/")
    ResponseEntity addAppointment(AppointmentDto appointmentDto);


}
