package rest.api;

import dto.DoctorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Adela on 12.03.2017.
 */
@RequestMapping("/doctors")
@CrossOrigin
public interface DoctorEndpoint {
    @GetMapping(value = "/{id}")
    ResponseEntity getDoctor(long id);

    @GetMapping(value = "/{hospitalName}/{hospitalCity}")
    ResponseEntity getDoctors(String hospitalName,String hospitalCity);

    @GetMapping(value = "/list")
    ResponseEntity getAllDoctors();

    @PostMapping(value = "/")
    ResponseEntity addDoctor(DoctorDto dto);

    @PutMapping(value = "/{id}")
    ResponseEntity updateDoctor(long id, DoctorDto dto);

    @DeleteMapping(value = "/{id}")
    ResponseEntity deleteDoctor(long id);
}
