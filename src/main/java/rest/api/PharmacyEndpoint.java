package rest.api;

import dto.Context;
import dto.PharmacyDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@RequestMapping("/pharmacies")
@CrossOrigin
public interface PharmacyEndpoint {
    @GetMapping(value = "/{id}")
    ResponseEntity getPharmacy(long id);

    //    @GetMapping
//    ResponseEntity getAllPharmaciesCity(String hospitalCity);

//    @GetMapping(value = "/convert/{pharmacyCity}/{type}")
//    ResponseEntity convert(String pharmacyCity, String type) throws IOException;
    @GetMapping(value = "/getfile")
    ResponseEntity convert() throws IOException;

    @GetMapping(value = "/list")
    ResponseEntity getAllPharmacies();

    @PostMapping(value = "/")
    ResponseEntity addPharmacy(PharmacyDto dto);

    @PutMapping(value = "/update/{id}")
    ResponseEntity updatePharmacy(long id, PharmacyDto dto);

    @DeleteMapping(value = "/delete//{id}")
    ResponseEntity deletePharmacy(long id);

    @GetMapping
    ResponseEntity getPharmaciesByCity(@RequestParam("city") String city) throws IOException;

}
