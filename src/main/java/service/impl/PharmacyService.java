package service.impl;

import dto.Context;
import dto.PharmacyDto;
import export.CSVConvertor;
import export.JsonConvertor;
import model.PharmacyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.PharmacyRepository;
import service.api.IPharmacyService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@Service
public class PharmacyService implements IPharmacyService {
    @Autowired
    private PharmacyRepository pharmacyRepository;

    public PharmacyDto getPharmacy(Long id) {
        return pharmacyRepository.exists(id) ? pharmacyRepository.findOne(id).toDto() : null;
    }

    public List<PharmacyDto> getAllPharmacies() {
        List<PharmacyEntity> eitities = (List<PharmacyEntity>) pharmacyRepository.findAll();

        return eitities.size() != 0 ? convertEntitisToDtos(eitities) : null;
    }

    private List<PharmacyDto> convertEntitisToDtos(List<PharmacyEntity> eitities) {
        List<PharmacyDto> dtos = new ArrayList<>();
        eitities.forEach(x -> dtos.add(x.toDto()));

        return dtos;
    }

    public PharmacyDto addPharmacy(PharmacyDto dto) {
        PharmacyEntity entity = new PharmacyEntity();
        entity.fromDto(dto);

        if (pharmacyRepository.findByName(dto.getName()).size() == 0 &&
                pharmacyRepository.findByCity(dto.getCity()).size() >= 0) {
            pharmacyRepository.save(entity);
            return entity.toDto();

        }
        return null;
    }

    public PharmacyDto updatePharmacy(Long id, PharmacyDto dto) {
        if (pharmacyRepository.exists(id)) {
            PharmacyEntity entity = pharmacyRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);

            return entity.toDto();
        }

        return null;
    }

    public void deletePharmacy(Long id) {
        pharmacyRepository.delete(id);
    }

    public boolean canDelete(long id) {
        return pharmacyRepository.exists(id);
    }

    @Override
    public List<PharmacyDto> getPharmaciesByCity(String city) {
        List<PharmacyEntity> pharmacyEntities = pharmacyRepository.findByCity(city);

        if (pharmacyEntities.size() > 0) {
            return convertEntitisToDtos(pharmacyEntities);
        }
        return null;
    }

    @Override
    public File convert(String pharmacyCity, String type) throws IOException {
        Context context;
        List<PharmacyEntity> pharmacies = pharmacyRepository.findByCity(pharmacyCity);
        List<PharmacyDto> dtos = getPharmaciesByCity(pharmacyCity);

        if (type.equals("Json")) {
            context = new Context(new JsonConvertor());
        } else {
            context = new Context(new CSVConvertor());
        }

        File file = context.convertToFormat(dtos);

        return file;
    }

}
