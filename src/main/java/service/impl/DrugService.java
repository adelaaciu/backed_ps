package service.impl;

import dto.DiseaseDto;
import dto.DrugDto;
import model.DiseaseEntity;
import model.DrugEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.DiseaseRepository;
import repository.DrugRepository;
import service.api.IDrugService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class DrugService implements IDrugService {
    private final DrugRepository drugRepository;
    private final DiseaseRepository diseaseRepository;

    @Autowired
    public DrugService(DrugRepository drugRepository, DiseaseRepository diseaseRepository) {
        this.drugRepository = drugRepository;
        this.diseaseRepository = diseaseRepository;
    }

    public DrugDto getDrug(Long id) {
        return drugRepository.exists(id) ? drugRepository.findOne(id).toDto() : null;
    }

    public DrugDto getDrug(String name) {
        DrugEntity entity = drugRepository.findByName(name);
        return entity != null ? entity.toDto() : null;
    }

    public DrugDto getMedicineByDisease(String name) {
        //DrugEntity entity = medicineRepository.findByDiseaseName(name);
        //return entity != null ? entity.toDto() : null;
        return null;
    }

    public List<DrugDto> getAllDrugs() {
        List<DrugEntity> entities = (List<DrugEntity>) drugRepository.findAll();
        return entities.size() != 0 ? convertEntitisToDtos(entities) : new ArrayList<>();
    }

    public DrugDto addDrug(DrugDto dto) {
        DrugEntity entity = new DrugEntity();
        Set<DiseaseEntity> diseaseEntities = getDrugEntityList(dto.getDiseases());
        entity.fromDto(dto);
        entity.setDiseaseEntities(diseaseEntities);

        if (drugRepository.findByName(dto.getName()) == null) {
            drugRepository.save(entity);
            return entity.toDto();
        }
        return null;

    }

    private Set<DiseaseEntity> getDrugEntityList(Set<DiseaseDto> dtos) {
        Set<DiseaseEntity> list = new HashSet<>();

        for (DiseaseDto d :
                dtos) {
            list.add(diseaseRepository.findByName(d.getName()));
        }
        return list;
    }

    public DrugDto updateDrug(Long id, DrugDto dto) {
        if (drugRepository.exists(id)) {
            DrugEntity entity = drugRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);
            drugRepository.save(entity);

            return entity.toDto();
        }

        return null;
    }

    public void deleteMedicine(Long id) {
        drugRepository.delete(id);
    }

    public boolean canDelete(long id) {
        return drugRepository.exists(id);
    }

    @Override
    public List<DrugDto> getMedicineByCategory(String category) {
        List<DrugEntity> entities = (List<DrugEntity>) drugRepository.findByCategory(category);
        return entities.size() != 0 ? convertEntitisToDtos(entities) : new ArrayList<>();

    }

    private List<DrugDto> convertEntitisToDtos(List<DrugEntity> entities) {
        List<DrugDto> dtos = new ArrayList<>();

        entities.forEach(x -> dtos.add(x.toDto()));

        return dtos;
    }


}
