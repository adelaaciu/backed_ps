package service.impl;

import dto.DoctorDto;
import dto.HospitalDto;
import model.DoctorEntity;
import model.HospitalEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.HospitalRepository;
import service.api.IHospitalService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class HospitalService implements IHospitalService {
    @Autowired
    private HospitalRepository hospitalRepository;

    public HospitalDto getHospital(Long id) {
        if (hospitalRepository.exists(id)) {
            HospitalEntity entity = hospitalRepository.findOne(id);
            return entity.toDto();
        }
        return null;
    }

//    public List<HospitalDto> getHospitalsByLocation(AddressDto address) {
//        AddressEntity addressEntity = new AddressEntity();
//        addressEntity.fromDto(address);
//        List<HospitalEntity> entities = hospitalRepository.getHospitalsByLocation(addressEntity);
//
//        return entities != null ? convertEntityToDto(entities) : null;
//    }


    public List<HospitalDto> getAllHospitals() {
        List<HospitalEntity> entities = (List<HospitalEntity>) hospitalRepository.findAll();

        return entities.size() > 0 ? convertEntityToDto(entities) : null;
    }

    private List<HospitalDto> convertEntityToDto(List<HospitalEntity> entities) {
        List<HospitalDto> dtos = new ArrayList<>();

        entities.forEach(x -> dtos.add(x.toDto()));
        return dtos;
    }


    public HospitalDto addHospital(HospitalDto dto) {
        HospitalEntity e = new HospitalEntity();
        e.fromDto(dto);

        if (hospitalRepository.findByName(dto.getName()).size() == 0 &&
                hospitalRepository.findByCity(e.getCity()).size() >= 0) {
            hospitalRepository.save(e);
            return e.toDto();
        }
        return null;
    }

    public HospitalDto updateHospital(Long id, HospitalDto dto) {
        if (hospitalRepository.exists(id)) {
            HospitalEntity entity = hospitalRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);
            hospitalRepository.save(entity);

            return entity.toDto();
        }

        return null;
    }

    public void deleteHospital(Long id) {
        hospitalRepository.delete(id);

    }

    public boolean canDelete(long id) {
        return hospitalRepository.exists(id);
    }

    public List<HospitalDto> getHospitalsByCity(String city) {
        List<HospitalEntity> hospitalsByCity = hospitalRepository.findByCity(city);
        if (hospitalsByCity.size() > 0) {
            return convertEntityToDto(hospitalsByCity);
        }
        return null;
    }

    public Set<DoctorDto> getDoctorsFromHospital(String name, String city) {
        Set<HospitalEntity> hospitalEntity = hospitalRepository.findByName(name);
        Set<DoctorDto> doctorDtos = new HashSet<>();

        for (HospitalEntity h : hospitalEntity) {
            if (h.getCity().equals(city)) {
                return convertDoctorEntitisToDtos(h.getDoctors());
            }
        }

        return null;
    }


    private Set<DoctorDto> convertDoctorEntitisToDtos(Set<DoctorEntity> doctors) {
        Set<DoctorDto> dtos = new HashSet<>();

        doctors.forEach(x -> dtos.add(x.toDto()));

        return dtos;
    }
}
