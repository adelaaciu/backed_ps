package service.impl;

import dto.SectorDto;
import model.SectorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.SectorRepository;
import service.api.ISectorService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class SectorService implements ISectorService {
    @Autowired
    private SectorRepository sectorRepository;
    
    public SectorDto getSector(Long id) {
        return sectorRepository.exists(id) ? sectorRepository.findOne(id).toDto() : null;
    }

    public List<SectorDto> getAllSectors() {
        List<SectorEntity> entities = (List<SectorEntity>) sectorRepository.findAll();
        return entities.size() != 0 ? convertEntitiesToDTOs(entities) : null;
    }

    public SectorDto addSector(SectorDto dto) {
        SectorEntity entity = new SectorEntity();
        entity.fromDto(dto);

        if (sectorRepository.findSectorByName(dto.getName()) == null) {
            sectorRepository.save(entity);
            return entity.toDto();
        }
        return null;
    }

    public SectorDto updateSector(Long id, SectorDto dto) {
        if (sectorRepository.exists(id)) {
            SectorEntity entity = sectorRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);
            sectorRepository.save(entity);

            return entity.toDto();
        }
        return null;
    }

    public void deleteSector(Long id) {
        sectorRepository.delete(id);
    }

    public boolean canDelete(long id) {
        return sectorRepository.exists(id);
    }

    private List<SectorDto> convertEntitiesToDTOs(List<SectorEntity> entities) {
        List<SectorDto> dtos = new ArrayList<>();

        entities.forEach(x -> {
            dtos.add(x.toDto());
        });
        return dtos;
    }
}
