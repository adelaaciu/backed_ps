package service.impl;

import dto.AppointmentDto;
import model.AppointmentEntity;
import model.HospitalEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.AppointmentRepository;
import repository.HospitalRepository;
import service.api.AppointmentService;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 * Created by Adela on 5/21/2017.
 */
@Service
public class AppointmentServiceImpl implements AppointmentService {

    private final
    AppointmentRepository appointmentRepository;
    private final HospitalRepository hospitalRepository;

    @Autowired
    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, HospitalRepository hospitalRepository) {
        this.appointmentRepository = appointmentRepository;
        this.hospitalRepository = hospitalRepository;
    }

    public AppointmentEntity getAppointment(long id) {
        return appointmentRepository.findOne(id);
    }

    public AppointmentEntity addAppointment(AppointmentDto appointmentDto) {
        AppointmentEntity appointmentEntity = new AppointmentEntity();
        appointmentEntity.fromDto(appointmentDto);

        System.out.println(appointmentEntity + "\n" );

        if(!isAvailable(appointmentDto.getHospitalName(), appointmentEntity.getDate()))
            return null;

        HospitalEntity hospital = hospitalRepository.findByNameAndCity(appointmentDto.getHospitalName(), appointmentDto
                .getHospitalCity());

        appointmentEntity.setHospital(hospital);

        appointmentEntity = appointmentRepository.save(appointmentEntity);

//        hospital.getAppointments().add(appointmentEntity);

//        hospitalRepository.save(hospital);

        return appointmentEntity;
    }

    private boolean isAvailable(String doctorName, LocalDateTime date) {
//        ZonedDateTime fixedDate = ZonedDateTime.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth(), date.getHour(), 0, 0, 0, date
//                .getZone());
            LocalDateTime fixedDate = LocalDateTime.now();
        AppointmentEntity check = appointmentRepository.findByDoctorNameAndDate(doctorName, fixedDate);

        return check == null;
    }
}
