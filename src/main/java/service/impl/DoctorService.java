package service.impl;

import dto.DoctorDto;
import model.DoctorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.DoctorRepository;
import service.api.IDoctorService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class DoctorService implements IDoctorService {
    @Autowired
    private DoctorRepository doctorRepository;

    public DoctorDto getDoctor(Long id) {
        DoctorEntity doctorEntity = doctorRepository.findOne(id);
        return doctorEntity != null ? doctorEntity.toDto() : null;
    }

    public List<DoctorDto> getDoctors(String hospital, String city) {
        List<DoctorEntity> doctors = doctorRepository.findAllByHospitalNameAndHospitalCity(hospital,city);
        return doctors != null ? convertEntitisToDtos(doctors) : null;
    }

    public List<DoctorDto> getAllDoctors() {
        List<DoctorEntity> doctors = (List<DoctorEntity>) doctorRepository.findAll();
        List<DoctorDto> dtos = convertEntitisToDtos(doctors);

        return dtos.size() > 0 ? dtos : null;
    }

    private List<DoctorDto> convertEntitisToDtos(List<DoctorEntity> doctors) {
        List<DoctorDto> dtos = new ArrayList<>();

        doctors.forEach(x -> dtos.add(x.toDto()));

        return dtos;
    }

    public DoctorDto addDoctor(DoctorDto dto) {

        if (doctorRepository.findByName(dto.getName()) != null) return null;

        DoctorEntity entity = new DoctorEntity();
        entity.fromDto(dto);
        entity = doctorRepository.save(entity);

        return entity.toDto();
    }

    public DoctorDto updateDoctor(Long id, DoctorDto dto) {
        if (doctorRepository.exists(id)) {
            DoctorEntity entity = doctorRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);
            return entity.toDto();
        }
        return null;
    }

    public void deleteDoctor(Long id) {
        doctorRepository.delete(id);
    }

    public boolean canDelete(Long id) {
        return doctorRepository.exists(id);
    }


}
