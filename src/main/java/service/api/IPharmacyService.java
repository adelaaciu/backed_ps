package service.api;

import dto.PharmacyDto;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@Service
public interface IPharmacyService {

    PharmacyDto getPharmacy(Long id);

    List<PharmacyDto> getAllPharmacies();

    PharmacyDto addPharmacy(PharmacyDto dto);

    PharmacyDto updatePharmacy(Long id, PharmacyDto dto);

    void deletePharmacy(Long id);

//    PharmacyDto getPharmacyByLocation(AddressDto address);

    boolean canDelete(long id);

    List<PharmacyDto> getPharmaciesByCity(String city);

    File convert(String pharmacy, String type) throws IOException;
}
