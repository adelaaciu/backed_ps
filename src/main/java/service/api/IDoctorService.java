package service.api;

import dto.DoctorDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IDoctorService {
    DoctorDto getDoctor(Long id);

    List<DoctorDto> getAllDoctors();

    DoctorDto addDoctor(DoctorDto dto);

    DoctorDto updateDoctor(Long id, DoctorDto dto);

    void deleteDoctor(Long id);

    boolean canDelete(Long id);

    List<DoctorDto> getDoctors(String name, String city);
}
