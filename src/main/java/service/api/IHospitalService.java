package service.api;

import dto.DoctorDto;
import dto.HospitalDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IHospitalService {
    HospitalDto getHospital(Long id);

    List<HospitalDto> getAllHospitals();

    HospitalDto addHospital(HospitalDto dto);

    HospitalDto updateHospital(Long id, HospitalDto dto);

    void deleteHospital(Long id);

    boolean canDelete(long id);

//    List<HospitalDto> getHospitalsByLocation(AddressDto address);

    List<HospitalDto> getHospitalsByCity(String city);

    Set<DoctorDto> getDoctorsFromHospital(String name, String city);
}
