package service.api;

import dto.AppointmentDto;
import model.AppointmentEntity;
import org.springframework.stereotype.Service;

/**
 * Created by Adela on 5/21/2017.
 */

@Service
public interface AppointmentService {

    AppointmentEntity getAppointment(long id);

    AppointmentEntity addAppointment(AppointmentDto appointmentDto);

}
