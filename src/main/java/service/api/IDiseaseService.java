package service.api;

import dto.DiseaseDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IDiseaseService {
    DiseaseDto getDisease(Long id);

    List<DiseaseDto> getAllDiseases();

    DiseaseDto addDisease(DiseaseDto dto);

    DiseaseDto updateDisease(Long id, DiseaseDto dto);

    void deleteDisease(Long id);

    boolean canDelete(Long id);

    DiseaseDto getDiseaseByName(String diseaseName);

    List<DiseaseDto> findAllByNameContaining(String name);

    List<DiseaseDto> getDiseaseByCategory(String category);
//    List<DiseaseDto> getDiseaseByMedicine(String medicineName);
}
