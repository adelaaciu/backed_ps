package service.api;

import dto.SectorDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface ISectorService {
    SectorDto getSector(Long id);

    List<SectorDto> getAllSectors();

    SectorDto addSector(SectorDto dto);

    SectorDto updateSector(Long id, SectorDto dto);

    void deleteSector(Long id);

    boolean canDelete(long id);
}
