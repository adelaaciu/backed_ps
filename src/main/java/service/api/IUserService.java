package service.api;

import dto.UserDto;
import model.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IUserService {

    UserDto save(UserEntity user);

    List<UserDto> getAll();
}
