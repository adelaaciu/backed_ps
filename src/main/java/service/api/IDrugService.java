package service.api;

import dto.DrugDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IDrugService {
    DrugDto getDrug(Long id);

    List<DrugDto> getAllDrugs();

    DrugDto addDrug(DrugDto dto);

    DrugDto updateDrug(Long id, DrugDto dto);

    void deleteMedicine(Long id);

    DrugDto getDrug(String name);

    DrugDto getMedicineByDisease(String name);

    boolean canDelete(long id);

    List<DrugDto> getMedicineByCategory(String category);
}
