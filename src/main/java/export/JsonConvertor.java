package export;

import com.google.gson.Gson;
import dto.PharmacyDto;

import java.io.*;
import java.util.List;

/**
 * Created by Adela on 28.05.2017.
 */
public class JsonConvertor implements Convertor {
    private static final String PATH_TO_FILE = "C:\\Users\\Adela\\Documents\\CTI III\\SEM II\\PS\\Proiect\\Fisiere\\fi.json";

    public File convert(List<PharmacyDto> pharmacies) throws IOException {
        File f = new File(PATH_TO_FILE);


        FileOutputStream fop = new FileOutputStream(f);

        if (!f.exists()) {
            f.createNewFile();
        }

        Gson gson = new Gson();
        String json = gson.toJson(pharmacies);

        byte[] contentInBytes = json.getBytes();

        fop.write(contentInBytes);
        fop.flush();
        fop.close();

        return f;
    }
}
