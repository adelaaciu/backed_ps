package export;

import dto.AppointmentDto;
import dto.PharmacyDto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Adela on 28.05.2017.
 */
public interface Convertor {
    File convert(List<PharmacyDto> pharmacies) throws IOException;
}
