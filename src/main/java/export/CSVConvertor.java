package export;

import dto.PharmacyDto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Adela on 28.05.2017.
 */
public class CSVConvertor implements Convertor {

    private static final String PATH_TO_FILE = "C:\\Users\\Adela\\Documents\\CTI III\\SEM II\\PS\\Proiect\\Fisiere\\fi.csv";

    public File convert(List<PharmacyDto> pharmacies) throws IOException {
        File f = new File(PATH_TO_FILE);

        FileOutputStream fop = new FileOutputStream(f);


        byte[] contentInBytes;

        String header = "\"name\",\"startHour\",\"endHour\",\"city\", \"address\"";


        contentInBytes = header.getBytes();
        fop.write(contentInBytes);

        for (PharmacyDto p : pharmacies) {
            String s = (String.format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"",
                    p.getName(),
                    p.getStartHour(),
                    p.getEndHour(),
                    p.getCity(),
                    p.getAddress()

            ));

            contentInBytes = s.getBytes();
            fop.write(contentInBytes);
        }


        fop.close();
        return f;
    }
}
