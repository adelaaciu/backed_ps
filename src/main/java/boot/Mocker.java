package boot;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import repository.*;
import service.api.IDrugService;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Adela on 01.05.2017.
 */
@Component
public class Mocker {


    final IDrugService medicineService;
    final DrugRepository drugRepository;
    final UserRepository userRepository;
    private DiseaseRepository diseaseRepository;
    private SectorRepository sectorRepository;
    private HospitalRepository hospitalRepository;
    private DoctorRepository doctorRepository;
    private PharmacyRepository pharmacyRepository;
    private DrugEntity drugEntity1;


    @Autowired
    public Mocker(IDrugService drugService, DrugRepository drugRepository, UserRepository userRepository, DiseaseRepository diseaseRepository, SectorRepository sectorRepository, HospitalRepository hospitalRepository, DoctorRepository doctorRepository, PharmacyRepository pharmacyRepository) {
        this.medicineService = drugService;
        this.drugRepository = drugRepository;
        this.userRepository = userRepository;
        this.diseaseRepository = diseaseRepository;
        this.sectorRepository = sectorRepository;
        this.hospitalRepository = hospitalRepository;
        this.doctorRepository = doctorRepository;
        this.pharmacyRepository = pharmacyRepository;
    }

    @PostConstruct
    public void init() {
        BCryptPasswordEncoder passwordEncoder =  new BCryptPasswordEncoder();

        UserEntity user = new UserEntity();
        user.setPassword(passwordEncoder.encode("admin"));
        user.setUsername("admin");
        user.setEmail("admin");
        user.setRole("ADMIN");
        userRepository.save(user);

        user = new UserEntity();
        user.setPassword(passwordEncoder.encode("user"));
        user.setUsername("user");
        user.setEmail("user");
        user.setRole("USER");
        userRepository.save(user);

        DiseaseEntity diseaseEntity = new DiseaseEntity();
        diseaseEntity.setName("disease");
        diseaseEntity.setCategory("inima");
        diseaseRepository.save(diseaseEntity);

        DiseaseEntity diseaseEntity1 = new DiseaseEntity();
        diseaseEntity1.setName("disease1");
        diseaseEntity1.setCategory("inima1");
        diseaseRepository.save(diseaseEntity1);


        SectorEntity sectorEntity = new SectorEntity();
        sectorEntity.setName("cardiologie");
        sectorEntity.setLevel("1");
        sectorEntity.setChambers(10);

        sectorRepository.save(sectorEntity);

        HospitalEntity hospitalEntity = new HospitalEntity();
        hospitalEntity.setName("spital1");
        hospitalEntity.setCity("Oradea");
        hospitalEntity.setAddress("Andrei");
        Set<SectorEntity> sectors = new HashSet<>();
        sectors.add(sectorEntity);
        hospitalEntity.setSectors(sectors);

        hospitalRepository.save(hospitalEntity);


        sectorEntity.setHospital(hospitalEntity);
        sectorRepository.save(sectorEntity);

        DoctorEntity doctorEntity = new DoctorEntity();
        doctorEntity.setName("Dr1");
        doctorEntity.setHospital(hospitalEntity);
        doctorEntity.setStartHour("10");
        doctorEntity.setEndHour("19");
        doctorEntity.setPhoneNo("0770274086");

        doctorRepository.save(doctorEntity);


        PharmacyEntity pharmacyEntity = new PharmacyEntity();
        pharmacyEntity.setName("p1");
        pharmacyEntity.setStartHour("12");
        pharmacyEntity.setEndHour("19");
        pharmacyEntity.setCity("Oradea");
        pharmacyEntity.setAddress("A1");

        pharmacyRepository.save(pharmacyEntity);

        DrugEntity drugEntity = new DrugEntity();
        drugEntity.setName("d1");
        drugEntity.setCategory("inima");
        drugEntity.setComposition("etx");
        drugEntity.getDiseaseEntities().add(diseaseEntity);
        drugEntity.getDiseaseEntities().add(diseaseEntity1);
        drugRepository.save(drugEntity);

        diseaseEntity.setDrugEntity(drugEntity);
        diseaseEntity1.setDrugEntity(drugEntity);
        diseaseRepository.save(diseaseEntity);
        diseaseRepository.save(diseaseEntity1);

        drugEntity1 = new DrugEntity();
        drugEntity1.setName("d1");
        drugEntity1.setCategory("plamani");
        drugEntity1.setComposition("etx");
        drugEntity1.getDiseaseEntities().add(diseaseEntity);
        drugRepository.save(drugEntity1);

        List<String> symptoms = new ArrayList<>();
        symptoms.add("symptom1");
        symptoms.add("symptom2");
        symptoms.add("symptom3");

        diseaseEntity.setSymptoms((ArrayList<String>) symptoms);
        diseaseEntity1.setSymptoms((ArrayList<String>) symptoms);
        diseaseRepository.save(diseaseEntity);
        diseaseRepository.save(diseaseEntity1);

        ArrayList<String> adverseReactions = new ArrayList<>();
        adverseReactions.add("adverseReaction1");
        adverseReactions.add("adverseReaction2");
        adverseReactions.add("adverseReaction3");

        drugEntity.setAdverseReactions(adverseReactions);
        drugEntity1.setAdverseReactions(adverseReactions);
        drugRepository.save(drugEntity1);
        drugRepository.save(drugEntity);

    }
}

